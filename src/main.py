
from random import randint

print("Welcome to the guessing game!")

guess = None
secret = randint(0,100)

while (guess != secret):
    guess = int(input(":"))
    if guess < secret:
        print("Higher!")
    else:
        print("Lower!")

print("You guessed it!")
